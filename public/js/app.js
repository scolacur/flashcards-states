var app = angular.module('flashCards', ['ui.router']);

app.value('currentFlashCards', []);

app.config(function($stateProvider){
  $stateProvider.state('flashCards', {
    url: '/flashCards',
    templateUrl:'/js/states/flashCards.html',
    controller: 'MainController'
  })
 .state('stats', {
    url: '/stats',
    templateUrl:'/js/states/stats.html',
    controller: 'StatsController'
  })
 .state('newCardForm', {
    url: '/newCard',
    templateUrl:'/js/states/newCard.html',
    controller: 'NewCardController'
  })


 .state('manageCard', {
    url: '/cards/:id/manageCard',
    templateUrl:'/js/states/manageCard.html',
    controller: function($scope, FlashCardsFactory, $stateParams) {
      var cardId = $stateParams.id;
      // var managing = false;
      FlashCardsFactory.getFlashCard(cardId)
      .then(function(card) {
        console.log(card);
        $scope.bob = card;
        // $scope.$parent.managing = true;
      });
    }
  })

 .state('manageCard.edit', {
    url: '/edit',
    templateUrl:'/js/states/editCard.html',
    controller: 'EditCardController'
  })

 .state('manageCard.delete', {
    url: '/delete',
    templateUrl:'/js/states/deleteCard.html',
    controller: function($scope, FlashCardsFactory, $stateParams, $state) {
      var cardId = $stateParams.id;
      FlashCardsFactory.removeCard(cardId)
      .then(function(card) {
        console.log(card);
        $state.go('flashCards');
      });
    }
  });

});


